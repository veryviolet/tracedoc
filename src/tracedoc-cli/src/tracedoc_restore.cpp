#include <iostream>
#include <vector>

#include <cxxopts.hpp>
#include <opencv2/opencv.hpp>
#include <annotations.h>


#include <tracedoc/geometry.h>
#include <tracedoc/enhancer.h>

using namespace std;
using namespace cv;

const string pref = "--> ";

class CPerformer
{
    CAnnotator original_annotator;
    CAnnotator source_annotator;
    Mat source;
    Mat target;
    Mat original;

    CTraceDocPointSet original_points;
    CTraceDocPointSet source_points;

    CTraceDocAreasSet original_texts;

    CTraceDocGeometry geometry;
    CTraceDocEnhancer enhancer;

    string targetname;
    string warpedname;

public:

    explicit CPerformer(const string & sourcename, const string & warpedname,
                        const string & targetname, const string & originalname)
    {
        this->targetname = targetname;
        this->warpedname = warpedname;

        original = imread(originalname);

        if (original.empty())
            throw runtime_error("failed to read original.");

        source = imread(sourcename);

        if (source.empty())
            throw runtime_error("failed to read source.");

        original_annotator = CAnnotator(originalname);
        source_annotator = CAnnotator(sourcename);


        original_annotator.GetPoints(original_points);
        original_annotator.GetTextRegions(original_texts);

        source_annotator.GetPoints(source_points);

        if ((original_points.GetPointsCount() < MINIMUM_POINTS_REQUIRED) ||
            (source_points.GetPointsCount() < MINIMUM_POINTS_REQUIRED))
            throw invalid_argument("not enought points.");

    };

    bool Process()
    {
        Mat processed = geometry.Process(source, source_points.get(), original, original_points.get());

        imwrite(warpedname, processed);

        enhancer.Process(processed, target, original_texts.get());

        imwrite(targetname, target);

        return true;
    }

};


int main(int argc, char *argv[]) {
    const string title = "TraceDoc: Restore.";

    cout << endl << endl;

    try {
        cxxopts::Options options("tracedoc_restore", title);

        options.add_options()
                ("s,source", "source file name", cxxopts::value<std::string>())
                ("o,original", "original file name", cxxopts::value<std::string>())
                ("w,warped", "warped file name", cxxopts::value<std::string>())
                ("t,target", "target file name", cxxopts::value<std::string>());

        auto result = options.parse(argc, argv);

        if (result.count("help") || !result.count("source")
            || !result.count("original") || !result.count("target") ) {
            cout << options.help({""}) << endl;
            exit(0);
        }

        cout << title << endl << endl;

        CPerformer performer(result["source"].as<string>(), result["warped"].as<string>(),
                             result["target"].as<string>(), result["original"].as<string>());

        performer.Process();

    }
    catch (const cxxopts::OptionException &e) {
        std::cout << pref << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
    catch (const std::exception &e) {
        std::cout << "error: " << e.what() << std::endl;
        exit(1);
    }


    return 0;
}