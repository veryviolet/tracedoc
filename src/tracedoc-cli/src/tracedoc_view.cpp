#include <iostream>
#include <vector>

#include <cxxopts.hpp>
#include <opencv2/opencv.hpp>
#include <annotations.h>

using namespace std;
using namespace cv;


const string pref = "--> ";


class CPerformer
{
    CAnnotator annotator;
    Mat im;
    Mat original;
    CTraceDocPointSet points;
    CTraceDocAreasSet texts;
public:

    explicit CPerformer(const string & fname) {
        original = imread(fname);

        if (original.empty())
            throw runtime_error("failed to read image.");

        annotator = CAnnotator(fname);

        annotator.GetPoints(points);
        annotator.GetTextRegions(texts);

    };

    void PlotMarker(int x, int y)
    {
        drawMarker(im, Point(x,y), Scalar(0,0,255), MARKER_CROSS, 40, 2);
    }

    void PlotPoint(int x, int y, unsigned short id) {
        drawMarker(im, Point(x, y), Scalar(0, 0, 255), MARKER_CROSS, 40, 2);
        putText(im, std::to_string(id), Point(x, y), cv::FONT_HERSHEY_PLAIN, 5.0, Scalar(0, 128, 0), 2);
    }

    void PlotTextArea(Rect r) {
        rectangle(im, r, Scalar(255, 0, 0), 2);
    }

    void ShowPoints()
    {
        im = original.clone();

        for (const auto &it : (PointsMapType)points)
            PlotPoint(it.second.x, it.second.y, it.first);

        for (const auto &it : texts.get())
            PlotTextArea(it.second);


        imshow("TraceDoc", im);
    }

    bool Start()
    {
        namedWindow("TraceDoc", cv::WINDOW_NORMAL);

        ShowPoints();

        return true;
    }

    bool ProcessKey(char c)
    {
        switch(c)
        {
            case 27:
                return false;
            default:
                return true;
        }
    }
};


int main(int argc, char *argv[]) {
    const string title = "TraceDoc: Markup.";

    cout << endl << endl;



    try {
        cxxopts::Options options("tracedoc_markup", title);

        options.add_options()
                ("f,file", "image file name", cxxopts::value<std::string>());

        auto result = options.parse(argc, argv);

        if (result.count("help") || !result.count("file")) {
            cout << options.help({""}) << endl;
            exit(0);
        }

        cout << title << endl << endl;

        CPerformer performer(result["file"].as<string>());

        performer.Start();

        for(;;)
        {
            auto c = waitKey();
            if(!performer.ProcessKey(c))
                break;
        }

    }
    catch (const cxxopts::OptionException &e) {
        std::cout << pref << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
    catch (const std::exception &e) {
        std::cout << "error: " << e.what() << std::endl;
        exit(1);
    }


    return 0;
}