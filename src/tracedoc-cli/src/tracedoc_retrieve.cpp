#include <iostream>
#include <vector>

#include <cxxopts.hpp>
#include <opencv2/opencv.hpp>
 #include <annotations.h>

#include <tracedoc/segmentation.h>
#include <tracedoc/enhancer.h>

using namespace std;
using namespace cv;

const string pref = "--> ";

class CPerformer {
    CAnnotator annotator;
    CAnnotator original_annotator;

    Mat source;
    Mat original;

    CTraceDocSegmentation segmentation;
    CTraceDocEnhancer enhancer;

public:

    explicit CPerformer(const string &sourcename, bool use_original, const string &originalname) {
        if (use_original) {
            original = imread(sourcename);

            if (original.empty())
                throw runtime_error("failed to read original.");

            original_annotator = CAnnotator(originalname);
        }

        source = imread(sourcename);

        if (source.empty())
            throw runtime_error("failed to read source.");

        annotator = CAnnotator(sourcename);

    };

    bool Process() {
        CTraceDocAreasSet words;

        segmentation.GetTextRegions(source, words);

        annotator.SetTextRegions(words);

        annotator.Save();

        return true;
    }

};


int main(int argc, char *argv[]) {
    const string title = "TraceDoc: Retrieve.";

    cout << endl << endl;

    try {
        cxxopts::Options options("tracedoc_retrieve", title);

        options.add_options()
                ("s,source", "source file name", cxxopts::value<std::string>())
                ("o,original", "original file name", cxxopts::value<std::string>());

        auto result = options.parse(argc, argv);

        if (result.count("help") || !result.count("source")) {
            cout << options.help({""}) << endl;
            exit(0);
        }

        cout << title << endl << endl;

        CPerformer performer(result["source"].as<string>(), (bool) result.count("original"),
                             result.count("original") ? result["source"].as<string>() : "");

        performer.Process();

    }
    catch (const cxxopts::OptionException &e) {
        std::cout << pref << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
    catch (const std::exception &e) {
        std::cout << "error: " << e.what() << std::endl;
        exit(1);
    }


    return 0;
}