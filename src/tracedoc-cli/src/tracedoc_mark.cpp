#include <cstdio>
#include <cstdlib>

#include <iostream>
#include <vector>

#include <cxxopts.hpp>
#include <opencv2/opencv.hpp>
#include <annotations.h>

#include <tracedoc/geometry.h>
#include <tracedoc/segmentation.h>

using namespace std;
using namespace cv;


const string pref = "--> ";

static void onMouse(int event, int x, int y, int flags, void *param);


class CPerformer {
    CTraceDocPointSet points;
    CTraceDocAreasSet text_areas;
    CAnnotator annotator;
    Mat im;
    Mat original;

    float min_warped_area = 0.5;

public:

    explicit CPerformer(const string &fname) {
        original = imread(fname);

        if (original.empty())
            throw runtime_error("failed to read image.");

        annotator = CAnnotator(fname);

        annotator.GetPoints(points);
        annotator.GetTextRegions(text_areas);

    };

    void IncreaseMinArea() {
        min_warped_area += 0.05;

        if (min_warped_area > 1.0)
            min_warped_area = 1.0;
    }

    void DecreaseMinArea() {
        min_warped_area -= 0.05;

        if (min_warped_area < 0.0)
            min_warped_area = 0.0;
    }

    void PlotPoint(int x, int y, unsigned short id) {
        drawMarker(im, Point(x, y), Scalar(0, 0, 255), MARKER_CROSS, 40, 2);
        putText(im, std::to_string(id), Point(x, y), cv::FONT_HERSHEY_PLAIN, 5.0, Scalar(0, 128, 0), 2);
    }

    void PlotTextArea(int l, int t, int r, int b) {
        rectangle(im, Rect(l, t, r, b), Scalar(255, 0, 0), 2);
    }

    void PlotMarkedTextArea(int l, int t, int r, int b, unsigned short id) {
        rectangle(im, Rect(l, t, r, b), Scalar(255, 0, 0), 2);
    }

    void ProcessAddPoint(int x, int y) {
        unsigned short id = points.AddPoint(x, y);
        PlotPoint(x, y, id);
        imshow("TraceDoc", im);
    }

    bool GetID(unsigned short hint, unsigned short & id) {

        FILE *fp;
        char idstr[64];
        char str[1035];
        bool found = false;

        sprintf(str, R"(/bin/zenity zenity --entry --text "ID" --entry-text "%d")", hint);

        fp = popen(str, "r");
        if (fp == nullptr)
            return false;

        while (fgets(idstr, sizeof(idstr)-1, fp) != nullptr) {
            if(strlen(idstr) != 0)
            {
                found = true;
                id = (unsigned short) std::stoi(idstr);
            }
        }

        pclose(fp);

        return found;
    }

    void ProcessAddPointAndSetID(int x, int y) {

        unsigned short hint = points.GetHintForID();
        unsigned short id = hint;

        if(!GetID(hint,id))
            return;

        points.AddPoint(x, y, id);

        PlotPoint(x, y, id);
        imshow("TraceDoc", im);
    }

    void ProcessDeletePoint(int x, int y) {
        int actual_x = 0, actual_y = 0;
        unsigned  short id = 0;
        if(!points.FindNearestPoint(x, y, id, actual_x, actual_y))
            return;
        points.RemovePoint(id);
        ShowPoints();
    }

    void ProcessAddTextArea(int x, int y) {
        //PlotMarker(x, y);
        //points.emplace_back(Point(x, y));
    }

    void ProcessAddTextAreaAndSetID(int x, int y) {
        //PlotMarker(x, y);
        //points.emplace_back(Point(x, y));
        //imshow("TraceDoc", im);
    }

    void ProcessDeleteTextArea(int x, int y) {
//        PlotMarker(x, y);
//        points.emplace_back(Point(x, y));
//        imshow("TraceDoc", im);
    }

    void ShowPoints() {
        im = original.clone();

        for (const auto &it : (PointsMapType)points)
            PlotPoint(it.second.x, it.second.y, it.first);

//        for (const auto &it : texts)
//            rectangle(im, it, Scalar(255, 0, 0), 2);

        imshow("TraceDoc", im);
    }

    bool Start() {
        namedWindow("TraceDoc", cv::WINDOW_NORMAL);

        ShowPoints();
        setMouseCallback("TraceDoc", onMouse, (void *) this);

        return true;
    }

    bool ProcessKey(char c) {
        switch (c) {
            case 27:
                return false;
            case -85: // "+" increase minimum area
            case 43: // "+" increase minimum area
                IncreaseMinArea();
                return true;
            case -83: // "-" increase minimum area
            case 45: // "-" decrease minimum area
                DecreaseMinArea();
                return true;
            case 103: // "g" auto generate grid of points
                points.ClearPoints();
                points.GenerateGrid(original, min_warped_area);
                ShowPoints();
                return true;
            case 97: // "a" auto generate adaptive set of points
                points.ClearPoints();
                points.GenerateAdaptiveGrid(original, min_warped_area);
                ShowPoints();
                return true;
            case 100: // "d" delete last added point
                ShowPoints();
                return true;
            case 115: // "s" save points
                annotator.SetPoints(points);
                annotator.Save();
                return true;
            case 99: // "c"
                points.ClearPoints();
                ShowPoints();
                return true;
            default:
                return true;
        }
    }
};

static void onMouse(int event, int x, int y, int flags, void *param) {
    if (event == EVENT_LBUTTONDOWN) {
        if (flags & EVENT_FLAG_SHIFTKEY)
            ((CPerformer *) param)->ProcessDeletePoint(x, y);
        else if (flags & EVENT_FLAG_CTRLKEY)
            ((CPerformer *) param)->ProcessAddPointAndSetID(x, y);
        else
            ((CPerformer *) param)->ProcessAddPoint(x, y);
    }
    else if (event == EVENT_RBUTTONDOWN) {
        if (flags | EVENT_FLAG_SHIFTKEY)
            ((CPerformer *) param)->ProcessDeleteTextArea(x, y);
        else if (flags | EVENT_FLAG_CTRLKEY)
            ((CPerformer *) param)->ProcessAddTextAreaAndSetID(x, y);
        else
            ((CPerformer *) param)->ProcessAddTextArea(x, y);
    }
}


int main(int argc, char *argv[]) {
    const string title = "TraceDoc: Markup.";

    cout << endl << endl;

    try {
        cxxopts::Options options("tracedoc_markup", title);

        options.add_options()
                ("f,file", "image file name", cxxopts::value<std::string>());

        auto result = options.parse(argc, argv);

        if (result.count("help") || !result.count("file")) {
            cout << options.help({""}) << endl;
            exit(0);
        }

        cout << title << endl << endl;

        CPerformer performer(result["file"].as<string>());

        performer.Start();

        for (;;) {
            auto c = waitKey();
            if (!performer.ProcessKey(c))
                break;
        }

    }
    catch (const cxxopts::OptionException &e) {
        std::cout << pref << "error parsing options: " << e.what() << std::endl;
        exit(1);
    }
    catch (const std::exception &e) {
        std::cout << "error: " << e.what() << std::endl;
        exit(1);
    }


    return 0;
}