#ifndef TRACEDOC_ANNOTATIONS_H
#define TRACEDOC_ANNOTATIONS_H

#include <string>
#include <iostream>
#include <fstream>
#include <streambuf>

#include <opencv2/opencv.hpp>

#include <json.hpp>

#include <tracedoc/geometry.h>
#include <tracedoc/segmentation.h>

using namespace std;
using namespace cv;
using json::JSON;

const string postfix = ".annotation";
const string default_annotation = "{\"points\": [], \"text\": [], \"images\": []}";

class CAnnotator
{
    string filename;
    JSON object;
public:
    CAnnotator(string fname="") {
        if(fname.empty())
            return;
        filename = fname + postfix;
        Load();
    };

    ~CAnnotator() = default;

    void Create() {
        string contents = default_annotation;
        object = JSON::Load(contents);
        Save();
    }

    void Load() {
        ifstream in(filename, std::ios::in | std::ios::binary);
        string contents;

        if (in)
        {
            in.seekg(0, std::ios::end);

            if(in.tellg() == 0)
            {
                Create();
                return;
            }
            else
            {
                in.seekg(0, std::ios::beg);

                std::stringstream buffer;
                buffer << in.rdbuf();

                contents= buffer.str();
            }
        }
        else {
            Create();
            return;
        }

        object = JSON::Load(contents);
    }

    bool Save() {
        ofstream out(filename, std::ios::out | std::ios::binary);

        if (out)
            out << object;

        return true;
    }

    unsigned long GetPoints(CTraceDocPointSet & lst) {
        lst.ClearPoints();

        for(int i=0; i<object["points"].size(); i++)
            lst.AddPoint((int) object["points"][i][0].ToInt(),
                         (int) object["points"][i][1].ToInt(),
                         (int) object["points"][i][2].ToInt());

        return lst.GetPointsCount();
    }

    void SetPoints(CTraceDocPointSet & lst) {
        JSON points("[]");

        for (const auto &it : (PointsMapType)lst)
        {
            JSON point("[]");
            point.append(it.second.x);
            point.append(it.second.y);
            point.append(it.first);
            points.append(point);

        }

        object["points"] = points;
    }

    int GetTextRegions(CTraceDocAreasSet & lst) {
        lst.Clear();

        for(int i=0; i<object["text"].size(); i++)
            lst.AddRect(Rect((int) object["text"][i][0].ToInt(), (int) object["text"][i][1].ToInt(),
                             (int) object["text"][i][2].ToInt(), (int) object["text"][i][3].ToInt()));

        return static_cast<int>(lst.get().size());
    }

    void SetTextRegions(CTraceDocAreasSet & lst) {
        JSON rects("[]");

        for (const auto &it : lst.get())
        {
            JSON rect("[]");
            rect.append(it.second.x);
            rect.append(it.second.y);
            rect.append(it.second.width);
            rect.append(it.second.height);
            rect.append(it.first);
            rects.append(rect);

        }

        object["text"] = rects;
    }

    int GetImageRegions(vector<Rect> & lst) {
        throw logic_error("not implemented");
    }

    void SetImageRegions(const vector<Rect> & lst) {
        throw logic_error("not implemented");
    }
};

#endif //TRACEDOC_ANNOTATIONS_H
