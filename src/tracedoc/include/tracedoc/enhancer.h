#ifndef LIBTRACEDOC_ENCHANCER_H
#define LIBTRACEDOC_ENCHANCER_H

#include <vector>
#include <opencv2/opencv.hpp>

#include "geometry.h"
#include "segmentation.h"


using namespace cv;
using namespace std;

#define TRACEDOC_MIN_UPPER_PYRAMID_SIZE 16
#define TRACEDOC_MIN_TEXTUAL_ELEMENT_LEVEL 4

#define TRACEDOC_MAD_NOISE_CELL 64

#define TRACEDOC_MAD_NOISE_MULT 8.0f

#define TRACEDOC_RECTS_INFLATE 1.3f

static const vector<float> DEFAULT_SIGMAS = {8,16,32};

class CTraceDocEnhancer
{
public:
    CTraceDocEnhancer();
    ~CTraceDocEnhancer();
    bool Process(Mat & src, Mat& dst, RectsMapType & texts);
private:
    bool Postprocess(Mat & src, Mat & dst);
    void BlankOutsideOfText(Mat & src, Mat& dst, RectsMapType & texts);
    float GetMedian(vector<float> & array);
    float GetNoiseLevel(Mat & src);
    void EntranceConvert(Mat & src, Mat & dst);
    void BinarizeOnThresholdsMap(Mat & src, Mat & dst, Mat & thrmap);
    void CreateThresholdsMap(Mat & src, Mat & dst, float noise);
    void ChooseChannel(Mat & src, Mat & dst);
    void IncreaseContrast(Mat & src, Mat & dst, float step);
    void WeightedBlur(Mat & src, Mat & dst, const vector<float> & sigmas);
    void SingleScaleRetinex(Mat & src, Mat & dst, float sigma = 0);
    void MultiScaleRetinex(Mat & src, Mat & dst, const vector<float> & sigmas = DEFAULT_SIGMAS);
    void ClaheCompensation(Mat & src, Mat & dst);
};


#endif