#ifndef TRACEDOC_JAVA_TOOLBOX_H
#define TRACEDOC_JAVA_TOOLBOX_H

#include <map>
#include <vector>

#include <jni.h>

#include "geometry.h"
#include "segmentation.h"

using namespace std;

jobjectArray points2javaArr(JNIEnv * env, PointsMapType const & points);

jobjectArray rects2javaArr(JNIEnv * env, RectsMapType const & rects);

PointsMapType javaArr2points(JNIEnv * env, jobjectArray javaArrOfArr);

RectsMapType javaArr2rects(JNIEnv * env, jobjectArray javaArrOfArr);

#endif // TRACEDOC_JAVA_TOOLBOX_H
