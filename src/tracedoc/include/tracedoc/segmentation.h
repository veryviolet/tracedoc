#ifndef LIBTRACEDOC_SEGMENTATION_H
#define LIBTRACEDOC_SEGMENTATION_H

#include <vector>
#include <iostream>

#include <opencv2/opencv.hpp>


using namespace cv;
using namespace std;

enum TRACEDOC_TEXT_METHOD
{
    EXTREMAL_REGIONS,
    CONTOURS
};

#define TRACEDOC_MAX_AREA_DEVIATION 10
#define TRACEDOC_MAX_RATIO 30

#define TRACEDOC_IOU_THRESHOLD 0.8

#define TRACEDOC_TEXT_IOU_THRESHOLD 0.7

typedef map<unsigned int, Rect> RectsMapType;

class CTraceDocAreasSet
{
    RectsMapType areas;
public:
    CTraceDocAreasSet() = default;
    ~CTraceDocAreasSet() = default;

    RectsMapType & get() { return areas; };

    bool ImportAreas(vector<vector<int> > & lst);
    vector<vector<int> > ExportAreas();
    void AddRect(Rect r);
    void AddRect(Rect r, unsigned short id);
    void Clear();
    bool IdentifyRects(RectsMapType & rects, RectsMapType & marked);
    vector<Rect> GetAreasInsideRect(Rect target);
private:
    float IOU(Rect &a, Rect &b);
    unsigned short GetHintForID();
    bool FindContainingRect(int x, int y, Rect & found);
    bool FindMaximumJaccardRect(Rect src, Rect &found, float &found_jaccard, float min_jaccard);
};


class CTraceDocSegmentation
{
public:
    CTraceDocSegmentation() = default;
    ~CTraceDocSegmentation() = default;
    bool GetTextRegions(Mat &src, CTraceDocAreasSet &target);
    bool GetAdaptiveTextRegions(Mat &src, CTraceDocAreasSet &original, CTraceDocAreasSet &target);
    bool GetImageRegions(Mat & src, vector<Rect> & target);
private:
    bool Preprocess(Mat & src, Mat & dst);
    bool GetTextRegionsContours(Mat &src, CTraceDocAreasSet &target);
};

#endif