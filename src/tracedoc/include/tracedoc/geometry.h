#ifndef LIBTRACEDOC_GEOMETRY_H
#define LIBTRACEDOC_GEOMETRY_H

#include <vector>
#include <opencv2/opencv.hpp>

#include "tracedoc/geometry.h"

#define MINIMUM_POINTS_REQUIRED 4

#define MIN_WARPED_AREA 0.3

#define TRACEDOC_KEYPOINT_THRESHOLD 250
#define TRACEDOC_KEYPOINTS_PER_CELL 3

using namespace cv;
using namespace std;

typedef map<unsigned int short, Point> PointsMapType;

class CTraceDocPointSet {
    PointsMapType points;

public:
    CTraceDocPointSet() = default;

    ~CTraceDocPointSet() = default;

    explicit operator PointsMapType & ();

    PointsMapType & get() {return points; };

    unsigned short GetHintForID();

    int GetPointsCount() { return (int) points.size(); };

    bool GenerateGrid(Mat &source, float min_warped = MIN_WARPED_AREA);

    bool GenerateAdaptiveGrid(Mat &source, float min_warped = MIN_WARPED_AREA);

    bool GenerateOnKeyPoints(Mat &source, float min_warped = MIN_WARPED_AREA);

    unsigned short AddPoint(int x, int y);

    bool AddPoint(int x, int y, unsigned short id);

    bool RemovePoint(unsigned short id);

    bool RemovePoint(int x, int y);

    bool FindNearestPoint(int x, int y, unsigned short & id, int & actual_x, int & actual_y);

    bool FindPoint(unsigned short id, int & found_x, int & found_y);

    void ClearPoints() { points.clear(); };
};

class CTraceDocGeometry {
public:
    CTraceDocGeometry();

    ~CTraceDocGeometry();

    Mat Process(Mat &source, PointsMapType &source_points,
                Mat &original, PointsMapType &original_points);
};


#endif