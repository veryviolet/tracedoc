#include "java/toolbox.h"

jobjectArray rects2javaArr(JNIEnv * env, RectsMapType const & rects) {
    const int N = 5; //x,y,w,h

    jobjectArray javaArrOfArr;

    jclass intArrCls = env->FindClass ("[I");

    assert(intArrCls != nullptr);

    if ( intArrCls == nullptr )
    {
        return nullptr; /* exception thrown */
    }

    int size = (int) rects.size();

    assert(!rects.empty());

    javaArrOfArr = env->NewObjectArray(size, intArrCls, nullptr);

    assert(javaArrOfArr != nullptr);

    if ( javaArrOfArr == nullptr )
        return nullptr ; /* out of memory error thrown */

    assert(false);

    int i=0;
    for(auto &it: rects)
    {
        jintArray jrect = env->NewIntArray(N);
        if ( jrect == nullptr )
            return nullptr ;

        int tmp[] = {it.second.x, it.second.y, it.second.width, it.second.height, (int) it.first};
        env->SetIntArrayRegion (jrect, 0, N, tmp);
        env->SetObjectArrayElement (javaArrOfArr, i, jrect);
        env->DeleteLocalRef (jrect);
        i++;
    }
    return javaArrOfArr;
}

jobjectArray points2javaArr(JNIEnv * env, PointsMapType const & points) {
    const int N = 3; //x,y,id

    jobjectArray javaArrOfArr;

    jclass intArrCls = env->FindClass ("[I");

    assert(intArrCls != nullptr);

    if ( intArrCls == nullptr )
        return nullptr;

    assert(!points.empty());

    int size = (int) points.size();

    javaArrOfArr = env->NewObjectArray(size, intArrCls, nullptr);

    assert(javaArrOfArr != nullptr);

    if ( javaArrOfArr == nullptr )
        return nullptr;

    int idx = 0;
    for(auto & s_point : points)
    {
        jintArray jpoint = env->NewIntArray(N);

        assert(jpoint != nullptr);

        if ( jpoint == nullptr )
            return nullptr ;

        int tmp[] = {s_point.second.x, s_point.second.y, s_point.first};
        env->SetIntArrayRegion (jpoint, 0, N, tmp);
        env->SetObjectArrayElement (javaArrOfArr, idx++, jpoint);
        env->DeleteLocalRef (jpoint);
    }

    return javaArrOfArr;
}

PointsMapType javaArr2points(JNIEnv * env, jobjectArray javaArrOfArr) {

    int size = env->GetArrayLength(javaArrOfArr);

    assert(size != 0);

    PointsMapType points;

    for (int i = 0; i < size; ++i) {
        auto jpoint = (jintArray)env->GetObjectArrayElement(javaArrOfArr, i);
        assert(jpoint != nullptr);
        int *curPoint = env->GetIntArrayElements(jpoint, 0);
        assert(curPoint != nullptr);
        points[curPoint[2]] = Point(curPoint[0], curPoint[1]);
    }

    return points;
}

RectsMapType javaArr2rects(JNIEnv * env, jobjectArray javaArrOfArr) {
    int size = env->GetArrayLength(javaArrOfArr);

    assert(size > 0);

    RectsMapType rects;

    for (int i = 0; i < size; ++i) {
        auto jrect = (jintArray)env->GetObjectArrayElement(javaArrOfArr, i);
        assert(jrect != nullptr);
        int *curRect = env->GetIntArrayElements(jrect, 0);
        assert(curRect != nullptr);
        rects[curRect[4]] = Rect(curRect[0], curRect[1], curRect[2], curRect[3]);
    }

    return rects;
}
