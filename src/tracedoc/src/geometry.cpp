#include "geometry.h"

#include <iostream>
#include <memory>
#include <algorithm>
#include <random>
#include "tracedoc/segmentation.h"

CTraceDocGeometry::CTraceDocGeometry() = default;

CTraceDocGeometry::~CTraceDocGeometry() = default;

Mat CTraceDocGeometry::Process(Mat &source, PointsMapType &source_points, Mat &original,
                               PointsMapType &original_points) {

    assert(!source.empty());
    assert(source.size() != Size(0, 0));

    assert(source_points.size() >= MINIMUM_POINTS_REQUIRED);

    if (source_points.size() < MINIMUM_POINTS_REQUIRED)
        throw invalid_argument("not enough points");

    assert(source_points.size() == original_points.size());

    if (source_points.size() != original_points.size())
        throw invalid_argument("point count must be the same.");

    std::shared_ptr<Point2f> src_points(new Point2f[source_points.size()]);
    std::shared_ptr<Point2f> dst_points(new Point2f[source_points.size()]);

    auto idx = 0;
    for (auto &s_point : source_points) {
        auto found = original_points.find(s_point.first);
        assert(found != original_points.end());
        if (found == original_points.end())
            throw invalid_argument("invalid point id");

        src_points.get()[idx] = s_point.second;
        dst_points.get()[idx] = found->second;

        idx++;
    }

    for (int i = 0; i < source_points.size(); i++) {
        src_points.get()[i] = source_points[i];
        dst_points.get()[i] = original_points[i];
    }

    auto trans = getPerspectiveTransform(src_points.get(), dst_points.get());

    assert(!trans.empty());

    Mat target = Mat(original.size(), original.type());

    warpPerspective(source, target, trans, target.size());

    return target;
}

bool CTraceDocPointSet::GenerateGrid(Mat &source, float min_warped) {

    if (min_warped <= 0.0 || min_warped >= 1.0)
        throw invalid_argument("min_warped must be a positive float < 1.0");

    auto width = source.cols;
    auto height = source.rows;

    auto points_per_cell = floor(std::min(width, height) * min_warped);

    auto n_col_points = int(width / points_per_cell) + 1;
    auto n_row_points = int(height / points_per_cell) + 1;

    unsigned short idx = 0;

    points.clear();

    for (int i = 0; i < n_col_points; i++)
        for (int j = 0; j < n_row_points; j++)
            points[idx++] = Point(int(i * points_per_cell), int(j * points_per_cell));

    return idx != 0;
}

bool CTraceDocPointSet::GenerateAdaptiveGrid(Mat &source, float min_warped) {

    CTraceDocSegmentation segmentation;
    CTraceDocAreasSet rects;

    Mat bw;

    if(source.channels() > 1)
        cvtColor(source, bw, cv::COLOR_BGR2GRAY);
    else
        bw = source;


    segmentation.GetTextRegions(bw, rects);
//    rects.SortAreas();


    if (min_warped <= 0.0 || min_warped >= 1.0)
        throw invalid_argument("min_warped must be a positive float < 1.0");

    auto width = source.cols;
    auto height = source.rows;

//    auto cols_per_cell = floor(std::min(width, height) * min_warped);
    auto cols_per_cell = floor(width * min_warped);
    auto rows_per_cell = floor(height * min_warped);

    auto n_col_cells = int(width / cols_per_cell);
    auto n_row_cells = int(height / rows_per_cell);

//    auto points_per_sub = floor(points_per_cell / TRACEDOC_KEYPOINTS_PER_CELL);
    auto cols_per_sub = floor(cols_per_cell / TRACEDOC_KEYPOINTS_PER_CELL);
    auto rows_per_sub = floor(rows_per_cell / TRACEDOC_KEYPOINTS_PER_CELL);


    unsigned short idx = 0;

    points.clear();
    auto rng = std::default_random_engine(0);

    for (int i = 0; i < n_col_cells; i++) {
        for (int j = 0; j < n_row_cells; j++) {
            for (int p = 0; p < TRACEDOC_KEYPOINTS_PER_CELL; p++) {
                for (int q = 0; q < TRACEDOC_KEYPOINTS_PER_CELL; q++) {

                    vector<Rect> localRects = rects.GetAreasInsideRect(
                            Rect(int(i * cols_per_cell + p*cols_per_sub),
                                 int(j * rows_per_cell + q*rows_per_sub),
                                 int(cols_per_sub), int(rows_per_sub)));

                    if(localRects.empty())
                        continue;

                    const unsigned long n = localRects.size();
                    unsigned long k = (std::rand() * n)/RAND_MAX;

                    AddPoint(localRects[k].x, localRects[k].y);
                }
            }
        }
    }

    return !points.empty();
}

bool CTraceDocPointSet::RemovePoint(unsigned short id) {
    auto it = points.find(id);
    auto found = it != points.end();

    if (found)
        points.erase(it);

    return found;
}

bool CTraceDocPointSet::RemovePoint(int x, int y) {
    throw logic_error("not implemented");
}

unsigned short CTraceDocPointSet::AddPoint(int x, int y) {

    auto new_id = GetHintForID();

    points[new_id] = Point(x, y);

    return new_id;
}

bool CTraceDocPointSet::FindNearestPoint(int x, int y, unsigned short &id, int &actual_x, int &actual_y) {
    // ооооочень плохой алгоритм. но поскольку это только для cli нужно, сойдет.

    unsigned short min_id = 0;
    float min_dist = 1e100;
    float dist = 0;

    if (points.size() == 0)
        return false;

    for (const auto &it: points) {
        dist = (x - it.second.x) * (x - it.second.x) + (y - it.second.y) * (y - it.second.y);
        if (dist < min_dist) {
            min_dist = dist;
            min_id = it.first;
        }
    }

    id = min_id;
    actual_x = points[min_id].x;
    actual_y = points[min_id].y;

    return true;
}

bool CTraceDocPointSet::FindPoint(unsigned short id, int &found_x, int &found_y) {
    if (points.find(id) == points.end())
        return false;

    Point pt = points[id];

    found_x = pt.x;
    found_y = pt.y;

    return true;
}

bool CTraceDocPointSet::AddPoint(int x, int y, unsigned short id) {

    if (points.find(id) != points.end())
        return false;

    points[id] = Point(x, y);

    return true;
}

CTraceDocPointSet::operator PointsMapType &() {
    return points;
}

unsigned short CTraceDocPointSet::GetHintForID() {
    auto new_id = (unsigned short) points.size();

    while (points.find(new_id) != points.end())
        new_id++;

    return new_id;
}

bool CTraceDocPointSet::GenerateOnKeyPoints(Mat &source, float min_warped) {

    if (min_warped <= 0.0 || min_warped >= 1.0)
        throw invalid_argument("min_warped must be a positive float < 1.0");

    auto width = source.cols;
    auto height = source.rows;

    auto points_per_cell = floor(std::min(width, height) * min_warped);

    auto n_col_points = int(width / points_per_cell);
    auto n_row_points = int(height / points_per_cell);

    auto n_col_key = int(points_per_cell / TRACEDOC_KEYPOINTS_PER_CELL);
    auto n_row_key = int(points_per_cell / TRACEDOC_KEYPOINTS_PER_CELL);


    unsigned short idx = 0;

    points.clear();

    Ptr<FastFeatureDetector> detector = FastFeatureDetector::create(TRACEDOC_KEYPOINT_THRESHOLD, true);

    for (int i = 0; i < n_col_points; i++) {
        for (int j = 0; j < n_row_points; j++) {
            vector<KeyPoint> kp;

            Mat roi = source(Rect(int(i * points_per_cell), int(j * points_per_cell),
                                  int(points_per_cell), int(points_per_cell)));

            detector->detect(roi, kp);

            auto rng = std::default_random_engine(0);
            std::shuffle(std::begin(kp), std::end(kp), rng);

//            std::sort( kp.begin(), kp.end(),
//                       [&]( const cv::KeyPoint& lhcs, const cv::KeyPoint& rhs )
//                       {
//                           return lhs.response > rhs.response;
//                       } );

            if (kp.size() == 0)
                continue;

            std::shared_ptr<bool> filled_key_cell(new bool[TRACEDOC_KEYPOINTS_PER_CELL]);

            for (const auto &it: kp) {
                auto p = int(it.pt.x / n_col_key);
                auto q = int(it.pt.y / n_row_key);

                if (filled_key_cell.get()[p + TRACEDOC_KEYPOINTS_PER_CELL * q])
                    continue;

                filled_key_cell.get()[p + TRACEDOC_KEYPOINTS_PER_CELL * q] = true;

                AddPoint(int(i * points_per_cell + it.pt.x), int(j * points_per_cell + it.pt.y));

            }
        }
    }

    return idx != 0;
}


