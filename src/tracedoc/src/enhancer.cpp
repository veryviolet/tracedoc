#include "enhancer.h"

#include <iostream>
#include <numeric>

CTraceDocEnhancer::CTraceDocEnhancer() = default;

CTraceDocEnhancer::~CTraceDocEnhancer() = default;

bool CTraceDocEnhancer::Process(Mat &src, Mat &dst, RectsMapType & texts) {

    Mat thrmap;
    Mat workable;
    Mat temp, temp2;

    EntranceConvert(src, workable);

    CreateThresholdsMap(workable, thrmap, GetNoiseLevel(workable));

    BinarizeOnThresholdsMap(workable, temp, thrmap);

    BlankOutsideOfText(temp, temp2, texts);

    Postprocess(temp2, dst);

    return true;
}



void CTraceDocEnhancer::SingleScaleRetinex(Mat &src, Mat &dst, float sigma)
{
    vector<float> sigmas = {sigma};

    MultiScaleRetinex(src, dst, sigmas);
}

void CTraceDocEnhancer::WeightedBlur(Mat &src, Mat &dst, const vector<float> &sigmas)
{
    dst = Mat(src.size(), src.type(), Scalar(0,0,0));
    Mat tmp;

    float weight = (float) 1.0/sigmas.size();

    for (const float & sigma: sigmas)
    {
        GaussianBlur(src,  tmp, Size(0, 0), sigma, sigma);
        dst = dst + weight*tmp;
    }
}

void CTraceDocEnhancer::MultiScaleRetinex(Mat &src, Mat &dst, const vector<float> & sigmas)
{
    Mat src_float, blurred, log_src, log_blurred, divided, divided_8u, tmp;
    src.convertTo(src_float, CV_32F);

    WeightedBlur(src_float, blurred, sigmas);

    cv::log(src_float, log_src);
    cv::log(blurred, log_blurred);


    tmp = 255*(log_src - log_blurred) + 127;

    tmp.convertTo(dst, CV_8UC1);

}

void CTraceDocEnhancer::ClaheCompensation(Mat &src, Mat &dst)
{
    Mat lab, trans;

    cvtColor(src, lab, cv::COLOR_BGR2Lab);

    vector<cv::Mat> lab_planes(3);
    split(lab, lab_planes);

    auto clahe = createCLAHE();
    clahe->setClipLimit(4);
    clahe->apply(lab_planes[0], trans);

    trans.copyTo(lab_planes[0]);
    merge(lab_planes, lab);

    cvtColor(lab, dst, cv::COLOR_Lab2BGR);

}

void CTraceDocEnhancer::IncreaseContrast(Mat &src, Mat &dst, float step)
{
    vector<Mat> rgb;
    split(src, rgb);
    Mat lut(1, 256, CV_8UC1);
    double contrastLevel = double(100 + step) / 100;
    uchar* p = lut.data;
    double d;
    for (int i = 0; i < 256; i++)
    {
        d = ((double(i) / 255 - 0.5)*contrastLevel + 0.5) * 255;
        if (d > 255)
            d = 255;
        if (d < 0)
            d = 0;
        p[i] = (uchar) d;
    }
    LUT(rgb[0], lut, rgb[0]);
    LUT(rgb[1], lut, rgb[1]);
    LUT(rgb[2], lut, rgb[2]);
    merge(rgb, dst);
}

void CTraceDocEnhancer::ChooseChannel(Mat &src, Mat &dst)
{

}

void CTraceDocEnhancer::CreateThresholdsMap(Mat &src, Mat &dst, float noise)
{
    vector<Mat> pyraMin, pyraMax, pyraMean;

    Mat currentMin = src.clone(), currentMax = src.clone(), currentMean = src.clone();

    while(min(currentMean.rows, currentMean.cols) > TRACEDOC_MIN_UPPER_PYRAMID_SIZE)
    {
        pyraMin.emplace_back(currentMin);
        pyraMax.emplace_back(currentMax);
        pyraMean.emplace_back(currentMean);

        Mat nextMin(currentMin.size()/2, currentMin.type());
        Mat nextMax(currentMax.size()/2, currentMax.type());
        Mat nextMean(currentMean.size()/2, currentMean.type());

        for(int y=0; y<currentMean.rows-2; y+=2)
        {
            for(int x=0; x<currentMean.cols-2; x+=2)
            {
                auto roiMin = {currentMin.ptr<float>(y)[x],
                               currentMin.ptr<float>(y)[x+1],
                               currentMin.ptr<float>(y+1)[x],
                               currentMin.ptr<float>(y+1)[x+1]};

                nextMin.ptr<float>(y/2)[x/2] = (float) std::min(roiMin);

                auto roiMax = {currentMax.ptr<float>(y)[x],
                               currentMax.ptr<float>(y)[x+1],
                               currentMax.ptr<float>(y+1)[x],
                               currentMax.ptr<float>(y+1)[x+1]};

                nextMax.ptr<float>(y/2)[x/2] = (float) std::max(roiMax);

                nextMean.ptr<float>(y/2)[x/2] = (float) 0.25 * (currentMean.ptr<float>(y)[x] +
                                                            currentMean.ptr<float>(y)[x+1] +
                                                            currentMean.ptr<float>(y+1)[x] +
                                                            currentMean.ptr<float>(y+1)[x+1]);
            }
        }

        currentMin = nextMin;
        currentMax = nextMax;
        currentMean = nextMean;


    }

    Mat thrmap = pyraMean[pyraMean.size()-1];


    for(auto i= pyraMean.size()-2; i>=TRACEDOC_MIN_TEXTUAL_ELEMENT_LEVEL; i--)
    {
        cv::pyrUp(thrmap, thrmap, pyraMean[i].size());
//        cv::resize(thrmap, thrmap, pyraMean[i].size(), 0,0, INTER_CUBIC);

        for(auto y = 0; y < thrmap.rows; y++)
        {
            for(auto x = 0; x < thrmap.cols; x++)
            {
                if(pyraMax[i].ptr<float>(y)[x] - pyraMin[i].ptr<float>(y)[x] > noise)
                {
                    thrmap.ptr<float>(y)[x] =  0.5f * (pyraMean[i].ptr<float>(y)[x] +
                            0.5f *(pyraMin[i].ptr<float>(y)[x] + pyraMax[i].ptr<float>(y)[x])
                                                      );
                }
            }
        }

    }

    for(auto i= TRACEDOC_MIN_TEXTUAL_ELEMENT_LEVEL-1; i>=0; i--)
        cv::resize(thrmap, thrmap, pyraMean[i].size(), 0,0, INTER_LINEAR);


    dst = thrmap.clone();
}

void CTraceDocEnhancer::BinarizeOnThresholdsMap(Mat &src, Mat &dst, Mat &thrmap)
{
    dst = src.clone();

    for(auto y = 0; y < src.rows; y++)
        for(auto x = 0; x < src.cols; x++)
            dst.ptr<float>(y)[x] = (src.ptr<float>(y)[x] <= thrmap.ptr<float>(y)[x]) ? 0.0f:255.0f;
}

void CTraceDocEnhancer::EntranceConvert(Mat &src, Mat &dst)
{
    Mat gray;

    if(src.channels() > 1)
        cvtColor(src, gray, cv::COLOR_BGR2GRAY);
    else
        gray = src;

    gray.convertTo(dst, CV_32F);
}

float CTraceDocEnhancer::GetNoiseLevel(Mat &src)
{
    vector<float> MAD;

    for(auto y=0; y< src.rows-TRACEDOC_MAD_NOISE_CELL; y+=TRACEDOC_MAD_NOISE_CELL)
    {
        for(auto x=0; x< src.cols-TRACEDOC_MAD_NOISE_CELL; x+=TRACEDOC_MAD_NOISE_CELL)
        {
            vector<float> oneMAD;
            for (auto ofs = 0; ofs < TRACEDOC_MAD_NOISE_CELL; ofs++)
            {
                oneMAD.insert(oneMAD.end(), src.ptr<float>(y+ofs) + x,
                              src.ptr<float>(y+ofs) + x + TRACEDOC_MAD_NOISE_CELL);
            }

            float med = GetMedian(oneMAD);

            for (float &el : oneMAD)
                el = abs(el-med);

            MAD.emplace_back(GetMedian(oneMAD));

        }

    }

    return 3.0f * TRACEDOC_MAD_NOISE_MULT * GetMedian(MAD);

}

float CTraceDocEnhancer::GetMedian(vector<float> & array)
{
    std::nth_element(array.begin(), array.begin() + array.size()/2, array.end());
    return array[array.size()/2];
}

void CTraceDocEnhancer::BlankOutsideOfText(Mat &src, Mat &dst, RectsMapType & texts) {

    assert(!src.empty());

    dst = Mat(src.size(), src.type());

    dst.setTo(255);

    for (auto &it: texts) {
        auto delta_x = (int) (0.5*it.second.width * (TRACEDOC_RECTS_INFLATE - 1));
        auto delta_y = (int) (0.5*it.second.height * (TRACEDOC_RECTS_INFLATE - 1));

        Point topleft = it.second.tl();
        Point bottomright = it.second.br();

        topleft.x = max(0, topleft.x - delta_x);
        topleft.y = max(0, topleft.y - delta_y);

        bottomright.x = min(dst.cols-1, bottomright.x + delta_x);
        bottomright.y = min(dst.rows-1, bottomright.y + delta_y);

        Rect newrect = Rect(topleft, bottomright);

        src(newrect).copyTo(dst(newrect));
    }

}

bool CTraceDocEnhancer::Postprocess(Mat &src, Mat &dst) {

    Mat connected;
    Mat morphKernel = getStructuringElement(MORPH_RECT, Size(4, 2));
    morphologyEx(src, dst, MORPH_CLOSE, morphKernel);
    morphologyEx(dst, dst, MORPH_OPEN, morphKernel);

//    Mat bw;
//
//    if(src.channels() > 1)
//        cvtColor(src, bw, CV_BGR2GRAY);
//    else
//        bw = src;
//
//    Mat grad;
//    Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));
//    morphologyEx(bw, grad, MORPH_GRADIENT, morphKernel);
//
//    Mat thr;
//    threshold(grad, thr, 128, 255, THRESH_BINARY);
//
//    Mat denoised;
//    morphKernel = getStructuringElement(MORPH_RECT, Size(5, 5));
//    morphologyEx(thr, denoised, MORPH_OPEN, morphKernel, Point(-1, -1), 2);
//
//    Mat connected;
//    morphKernel = getStructuringElement(MORPH_RECT, Size(9, 1));
//    morphologyEx(thr, connected, MORPH_CLOSE, morphKernel);
//
//    Mat mask = Mat::zeros(connected.size(), CV_8UC1);
//    Mat ready;
//
//    connected.convertTo(dst, CV_8UC1);
    return true;
}
