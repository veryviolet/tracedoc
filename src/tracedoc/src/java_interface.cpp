#include "java/interface.h"
#include "java/toolbox.h"
#include "geometry.h"
#include "enhancer.h"
#include "segmentation.h"

JNIEXPORT jobjectArray JNICALL Java_su_secret_tracedoc_TracedocAdapter_retrieveOrigin(JNIEnv *env,
                                                                                      jobject thisObj,
                                                                                      jstring originalName)
{
    auto originalNameString = env->GetStringUTFChars(originalName, nullptr);

    CTraceDocSegmentation segmentation;
    CTraceDocAreasSet rects;

    try
    {
        Mat original = imread(originalNameString);
        segmentation.GetTextRegions(original, rects);
    }
    catch(Exception & e)
    {
        cerr << e.what();
        return nullptr;
    }

    return rects2javaArr(env, rects.get());
}

JNIEXPORT jobjectArray JNICALL Java_su_secret_tracedoc_TracedocAdapter_retrievePhoto(JNIEnv *env,
                                                                                     jobject thisObj,
                                                                                     jstring photoName,
                                                                                     jstring originalName,
                                                                                     jobjectArray photoMarks,
                                                                                     jobjectArray originalMarks)
{
    auto originalNameString = env->GetStringUTFChars(originalName, nullptr);
    auto photoNameString = env->GetStringUTFChars(photoName, nullptr);

    auto photo_marks_map = javaArr2points(env,photoMarks);
    auto original_marks_map = javaArr2points(env, originalMarks);

    CTraceDocSegmentation segmentation;
    CTraceDocGeometry geometry;
    CTraceDocEnhancer enhancer;
    CTraceDocAreasSet original_rects, photo_rects;
    RectsMapType marked;

    try
    {
        Mat target;
        Mat original = imread(originalNameString);
        Mat photo = imread(photoNameString);
        segmentation.GetTextRegions(original, original_rects);
        Mat processed = geometry.Process(photo, photo_marks_map, original, original_marks_map);
        enhancer.Process(processed, target, original_rects.get());
        segmentation.GetTextRegions(target, photo_rects);
        original_rects.IdentifyRects(photo_rects.get(), marked);
    }
    catch(Exception & e)
    {
        cerr << e.what();
        return nullptr;
    }

    return rects2javaArr(env, marked);
}


JNIEXPORT jobjectArray JNICALL Java_su_secret_tracedoc_TracedocAdapter_getOriginalMarks(JNIEnv *env,
                                                                                        jobject thisObj,
                                                                                        jstring originalName,
                                                                                        jfloat minWarpedArea)
{
    auto originalNameString = env->GetStringUTFChars(originalName, nullptr);

    CTraceDocPointSet pointSet;

    Mat original = imread(originalNameString);

    pointSet.GenerateAdaptiveGrid(original, minWarpedArea);

    return points2javaArr(env, pointSet.get());
}


JNIEXPORT jobjectArray JNICALL Java_su_secret_tracedoc_TracedocAdapter_getOriginalMarksGrid(JNIEnv *env,
                                                                                        jobject thisObj,
                                                                                        jstring originalName,
                                                                                        jfloat minWarpedArea)
{
    auto originalNameString = env->GetStringUTFChars(originalName, nullptr);

    CTraceDocPointSet pointSet;

    Mat original = imread(originalNameString);

    pointSet.GenerateGrid(original, minWarpedArea);

    return points2javaArr(env, pointSet.get());
}
