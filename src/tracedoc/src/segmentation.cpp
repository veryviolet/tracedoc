#include "segmentation.h"

bool CTraceDocSegmentation::GetTextRegions(Mat &src, CTraceDocAreasSet &target)
{
    return GetTextRegionsContours(src, target);
}

bool CTraceDocSegmentation::GetImageRegions(Mat &src, vector<Rect> & target)
{
    return true;
}

bool CTraceDocSegmentation::GetTextRegionsContours(Mat &src, CTraceDocAreasSet &target)
{
    Mat ready;

    Preprocess(src, ready);

    int tp = ready.type();

    vector<vector<Point>> contours;
    vector<Vec4i> hierarchy;

    cv::findContours(ready, contours, cv::RETR_EXTERNAL, cv::CHAIN_APPROX_SIMPLE, Point(0, 0));

    assert(!contours.empty());

    target.Clear();

    float mean = 0, std = 0;
    vector<Rect> allrects;

    for(const vector<Point> & contour: contours)
        allrects.push_back(boundingRect(contour));


    for(const Rect & r: allrects)
        mean += r.area();

    assert(mean!=0);

    mean /= contours.size();

    for(const Rect & r: allrects)
    {
        if(r.area() < mean*TRACEDOC_MAX_AREA_DEVIATION and r.area() > mean/TRACEDOC_MAX_AREA_DEVIATION and
                max(r.width, r.height)/min(r.width, r.height) < TRACEDOC_MAX_RATIO)
            target.AddRect(r);
    }

    return true;

}

bool CTraceDocSegmentation::Preprocess(Mat &src, Mat &dst) {

    assert(!src.empty());
    assert(src.size() != cv::Size(0,0));

    Mat bw;

    if(src.channels() > 1)
        cvtColor(src, bw, cv::COLOR_BGR2GRAY);
    else
        bw = src;

    Mat grad;
    Mat morphKernel = getStructuringElement(MORPH_ELLIPSE, Size(3, 3));
    morphologyEx(bw, grad, MORPH_GRADIENT, morphKernel);

    Mat thr;
    threshold(grad, thr, 128, 255, THRESH_BINARY);

    Mat denoised;
    morphKernel = getStructuringElement(MORPH_RECT, Size(5, 5));
    morphologyEx(thr, denoised, MORPH_OPEN, morphKernel, Point(-1, -1), 2);

    Mat connected;
    morphKernel = getStructuringElement(MORPH_RECT, Size(9, 1));
    morphologyEx(thr, connected, MORPH_CLOSE, morphKernel);

    Mat ready;

    connected.convertTo(dst, CV_8UC1);

    return true;
}

bool CTraceDocSegmentation::GetAdaptiveTextRegions(Mat &src, CTraceDocAreasSet &original, CTraceDocAreasSet &target) {
    return false;
}

//    Rect found_rect = Rect(0,0,0,0);
//    unsigned short found_id = 0;
//
//    if(FindContainingRectMarked(x, y, found_id))
//    {
//        auto rect_to_remark = marked_text_areas[found_id];
//        marked_text_areas.erase(found_id);
//        areas[rect_to_remark] = id;
//
//        return true;
//    }
//    else if(FindContainingRect(x, y, found_rect))
//    {
//        areas[found_rect] = id;
//        marked_text_areas[id] = found_rect;
//        return true;
//    }
//    else
//        return false;

//
//    unsigned short found_id = 0;
//
//    if(FindContainingRectMarked(x, y, found_id))
//    {
//        auto rect_to_remove = marked_text_areas[found_id];
//        marked_text_areas.erase(found_id);
//        areas[rect_to_remove] = 0;
//        return true;
//    }
//    else
//        return false;
//

bool CTraceDocAreasSet::ImportAreas(vector<vector<int> > &lst) {

    areas.clear();

    for(const vector<int> & one: lst)
    {
        assert(one.size()>=4);

        if(one.size() == 4)
        {
            auto id = GetHintForID();
            areas[id] = Rect(one[0], one[1], one[2], one[3]);

        } else
        {
            areas[one[4]] = Rect(one[0], one[1], one[2], one[3]);
        }
    }

    return !areas.empty();
}

vector<vector<int> > CTraceDocAreasSet::ExportAreas() {

    vector<vector<int> > exported;

    for(const auto &it: areas)
        exported.emplace_back(vector<int>({it.second.x, it.second.y, it.second.width, it.second.height,
                                           (int)it.first}));

    return exported;
}

bool CTraceDocAreasSet::FindContainingRect(int x, int y, Rect &found) {
    return false;
}

bool CTraceDocAreasSet::FindMaximumJaccardRect(Rect src, Rect &found, float &found_jaccard, float min_jaccard) {
    return false;
}

void CTraceDocAreasSet::AddRect(const Rect r) {
    areas[GetHintForID()] = r;
}

void CTraceDocAreasSet::Clear() {
    areas.clear();
}

vector<Rect> CTraceDocAreasSet::GetAreasInsideRect(Rect target) {
    throw logic_error("Not implemented");
}

void CTraceDocAreasSet::AddRect(Rect r, unsigned short id) {
    areas[id] = r;
}

bool CTraceDocAreasSet::IdentifyRects(RectsMapType &rects, RectsMapType & marked) {

    marked.clear();

    for(auto &src: rects)
    {
        Point c = (src.second.tl() + src.second.br())/2;

        for(auto &it: areas)
        {
            if(it.second.contains(c))
            {
                if(IOU(src.second, it.second) > TRACEDOC_IOU_THRESHOLD)
                {
                    marked[it.first] = src.second;
                }
            }
        }
    }

    return marked.size() == rects.size();
}

unsigned short CTraceDocAreasSet::GetHintForID() {
    auto new_id = (unsigned short) areas.size();

    while (areas.find(new_id) != areas.end())
        new_id++;

    return new_id;
}

float CTraceDocAreasSet::IOU(Rect &a, Rect &b) {
    auto intr = (float) (a & b).area();
    return intr/(a.area() + b.area() - intr);
}

