#!/bin/sh

SCRIPTPATH="$( cd "$(dirname "$0")" ; pwd -P )"

sudo docker build -t tracedoc -f Dockerfile $SCRIPTPATH
