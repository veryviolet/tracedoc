FROM ubuntu

ENV DEBIAN_FRONTEND noninteractive

RUN apt-get update
RUN apt-get install -y build-essential checkinstall cmake pkg-config git
RUN apt-get install -y openjdk-8-jdk gradle
RUN apt-get install -y libjpeg-dev libpng-dev libtiff-dev libgtk2.0-dev libeigen3-dev
RUN cd ~; git clone https://github.com/opencv/opencv.git
RUN cd ~/opencv; git checkout 3.4.2; mkdir build;
RUN cd ~/opencv/build; cmake -D CMAKE_BUILD_TYPE=RELEASE -D CMAKE_INSTALL_PREFIX=/usr/local ..
RUN cd ~/opencv/build; make -j 11; make install

RUN mkdir /opt/tracedoc

COPY . /opt/tracedoc

RUN mkdir /opt/tracedoc/b

WORKDIR /opt/tracedoc/

RUN mkdir /opt/tracedoc/build; cd /opt/tracedoc/build; cmake ..; make; make install

RUN ldconfig; ldconfig -p | grep tracedoc

WORKDIR /opt/tracedoc/java

ENV LD_LIBRARY_PATH = $LD_LIBRARY_PATH:/usr/local/lib

RUN gradle build

# RUN rm -rf /opt/tracedoc
